import requests
import json
from .keys import PEXELS_API_KEY


# Pexels API request function
def get_photo(city, state):

    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": f"{city} {state}"}
    response = requests.get(
        "https://api.pexels.com/v1/search", params=params, headers=headers
    )
    content = json.loads(response.content)
    try:
        return content["photos"][0]["src"]["original"]
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    latlonresponse = requests.get(
        f"https://api.openweathermap.org/data/2.5/weather?q={city},{state},US&limit=1%units=imperial&appid={OPEN_WEATHER_API_KEY}"
    )
    latlon = json.loads(latlonresponse.content)
    if latlon["cod"] == "404":
        return {
            "temp": None,
            "description": None,
        }
    temp_K = latlon["main"]["temp"]
    desc = latlon["weather"][0]["description"]
    temp_F = round(((temp_K - 273.15) * 1.8 + 32), 2)
    try:
        return {
            "temp": temp_F,
            "description": desc,
        }
    except (KeyError, IndexError):
        return None
